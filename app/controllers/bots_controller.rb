class BotsController < ApplicationController
  before_action :authenticate_customer!
  before_action :current_bot, only: %i[show update update_components destroy]
  before_action :check_access, only: %i[show update update_components destroy]

  rescue_from ActionController::ParameterMissing, with: :empty_form
  rescue_from RuntimeError, with: :forbidden
  rescue_from Telegram::Bot::Exceptions::ResponseError, with: :forbidden

  def index
    sorted_bots = current_customer.bots.sort_by { |bot| bot[:id] }
    render json: sorted_bots
  end

  def show
    sorted_screens = @bot.screens.map(&:with_components_by_priority).sort_by { |e| e[:priority] }
    render json: {
      bot: @bot,
      screens: sorted_screens
    }, status: 200
  end

  def create
    bot = current_customer.bots.create(name: 'New Bot')
    render json: bot, status: 200
  end

  def update
    raise 'Invalid token' until valid_token?
    @bot.assign_attributes(bot_params)
    @bot.save!
    post_hook_to_telegram if @bot.hook != current_hook
    render json: 'Success!', status: 200
  end

  def update_components
    updated_components_params.each { |component| update_component(component) }
    render json: 'Success!', status: 200
    Dispatcher.reset_bot(@bot.hook)
  end

  def destroy
    @bot.destroy
    render json: 'Success!', status: 200
  end

  def empty_form
    render json: 'Nothing to do there', status: 200
  end

  private

  def current_bot
    @bot = Bot.find(bot_id)
  end

  def bot_id
    params.require(:id)
  end

  def bot_params
    params.permit :token
  end

  def updated_components_params
    params.require(:changed_elements).map do |component|
      component.permit   :id,
                         :component_type,
                         :priority,
                         :screen_id,
                         :stop,
                         :text,
                         :variable_name,
                         :next_screen,
                         data: {}
    end
  end

  def accessible_params(component_params, type)
    message = %w[text stop]
    keyboard = %w[text data]
    user_input = %w[variable_name]
    go_to = %w[next_screen]
    accepted_keys = {
      'Message'   => message,
      'Keyboard'  => keyboard,
      'UserInput' => user_input,
      'GoTo' => go_to
    }
    accepted_params = component_params.to_h
    accepted_params.each_key { |k| accepted_params.delete(k) if accepted_keys[type].exclude?(k) }
  end

  def update_component(component_params)
    type = component_params[:component_type].split(/::/)[1]
    screen_element_attributes = {
      priority: component_params[:priority],
      screen_id: component_params[:screen_id]
    }
    component = "Components::#{type}".singularize.classify.constantize.find(component_params[:id])

    update_screen_element(component, screen_element_attributes)

    new_attributes = accessible_params(component_params, type)
    component.assign_attributes(new_attributes)
    component.save!
  end

  def update_screen_element(component, attributes)
    screen_element = component.screen_element
    screen_element.assign_attributes(attributes)
    screen_element.save!
  end

  def check_access
    raise 'Not allowed!' if current_customer.id != @bot.customer.id
  end

  def valid_token?
    response = BotEngine::Exec.get_me(bot_params[:token])
    @bot.name = response['result']['first_name']
    @bot.save!
    response['ok']
  end

  def current_hook
    response = BotEngine::Exec.get_webhook_info(@bot.token)
    response['result']['url'].split('/').last
  end

  def post_hook_to_telegram
    BotEngine::Exec.set_webhook(@bot.token, @bot.hook)
  end

  def forbidden(exception)
    pp exception
    render json: exception.message, status: 403
  end
end
