class CustomersController < ApplicationController
  before_action :authenticate_customer!

  def last_customer
    customer = current_customer
    render json: customer
  end
end
