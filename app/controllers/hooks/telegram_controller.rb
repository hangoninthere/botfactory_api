class Hooks::TelegramController < ApplicationController
  def update
    update = Telegram::Bot::Types::Update.new(telegram_params)
    hook = params_hook

    response = Dispatcher.update(hook, update)
    render nothing: true, status: 200
  end

  private

  def params_hook
    params.permit(:hook).require(:hook)
  end

  def telegram_params
    params.require(:telegram).permit!
  end
end
