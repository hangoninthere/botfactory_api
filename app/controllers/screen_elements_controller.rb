class ScreenElementsController < ApplicationController
  before_action :authenticate_customer!
  before_action :current_bot, :current_screen, only: %i[create]
  before_action :check_access, only: %i[create]

  rescue_from RuntimeError, with: :forbidden

  def create
    component = accepted_params[:component_type].singularize.classify.constantize.new
    element = @screen.screen_elements.new(component: component)
    element.save!
    render json: element.staffed_component, status: 200
  end

  private

  def current_bot
    @bot = Bot.find(accepted_params[:bot_id])
  end

  def current_screen
    @screen = Screen.find(accepted_params[:screen_id])
  end

  def check_access
    raise 'Not allowed!' if current_customer.id != @bot.customer.id
  end

  def forbidden(exception)
    pp exception
    render json: exception.message, status: 403
  end

  def accepted_params
    params.permit :component_type,
                  :screen_id,
                  :bot_id
  end
end
