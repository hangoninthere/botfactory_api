class ScreensController < ApplicationController
  before_action :authenticate_customer!
  before_action :current_bot, only: %i[create]

  def create
    screen = @bot.screens.create if current_customer == @bot.customer
    screen = screen.as_json.merge(screen_elements: [])
    render json: { screen: screen }, status: 200
  end

  def destroy
    Screen.find(screen_id).destroy
    render json: 'Success!', status: 200
  end

  private

  def current_bot
    @bot = Bot.find(params.require(:bot_id))
  end

  def screen_id
    params.require(:id)
  end
end
