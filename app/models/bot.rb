class Bot < ApplicationRecord
  belongs_to :customer
  has_many :screens, dependent: :destroy

  before_validation :set_hook
  before_create :build_associations
  after_save :first_screen_id

  private

  def build_associations
    screens.build(name: 'First screen', is_first: true)
  end

  def first_screen_id
    update_attribute('first_screen', screens.first.id)
  end

  def set_hook
    self.hook = generate_hook
  end

  def generate_hook
    loop do
      hook = SecureRandom.hex(16)
      break hook unless Bot.where(hook: hook).exists?
    end
  end
end
