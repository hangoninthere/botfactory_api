module Components
  class AbstractComponent < ApplicationRecord
    self.abstract_class = true
    before_create :update_class

    has_one :screen_element, as: :component
    has_one :screen, through: :screen_elements

    private

    def update_class
      self.component_type = self.class.name
    end
  end
end
