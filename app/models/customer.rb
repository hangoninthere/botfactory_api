class Customer < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  # :confirmable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  validates :email, presence: true
  validates :encrypted_password, presence: true

  has_many :bots, dependent: :destroy
end
