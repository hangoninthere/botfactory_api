class Screen < ApplicationRecord
  belongs_to :bot
  has_many :screen_elements, dependent: :destroy
  has_many :messages, through: :screen_elements, source: :component, source_type: 'Components::Message'
  has_many :keyboards, through: :screen_elements, source: :component, source_type: 'Components::Keyboard'
  has_many :user_inputs, through: :screen_elements, source: :component, source_type: 'Components::UserInput'
  has_many :go_tos, through: :screen_elements, source: :component, source_type: 'Components::GoTo'

  validates :bot_id, presence: true

  before_create :set_priority

  def with_components_by_priority
    data = { id: id, bot_id: bot_id, name: name, priority: priority }
    data.merge(screen_elements: screen_elements.map(&:staffed_component).sort_by { |e| e[:priority] })
  end

  private

  def set_priority
    self.priority = bot.screens.count + 1
  end
end
