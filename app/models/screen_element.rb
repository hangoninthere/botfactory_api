class ScreenElement < ApplicationRecord
  belongs_to :screen
  belongs_to :component, polymorphic: true, dependent: :destroy

  before_create :set_priority

  def staffed_component
    component.as_json.merge(priority: priority, screen_id: screen_id)
  end

  private

  def set_priority
    self.priority = screen.screen_elements.count + 1
  end
end
