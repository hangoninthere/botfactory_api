module BotEngine
  module Actions
    class GetMe
      def self.call(token)
        Telegram::Bot::Client.run(token) do |bot|
          bot.api.get_me(token: token)
        end
      end
    end
  end
end
