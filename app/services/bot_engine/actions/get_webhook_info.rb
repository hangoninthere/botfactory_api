module BotEngine
  module Actions
    class GetWebhookInfo
      def self.call(token)
        Telegram::Bot::Client.run(token) do |bot|
          bot.api.get_webhook_info(token: token)
        end
      end
    end
  end
end
