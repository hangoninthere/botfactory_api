module BotEngine
  module Actions
    class SendMessage
      class << self
      def send_message(token, channel, text, markup = nil)
        Telegram::Bot::Client.run(token) do |bot|
          bot.api.send_message(chat_id: channel, text: text, reply_markup: markup)
        end
      end
      end
    end
  end
end
