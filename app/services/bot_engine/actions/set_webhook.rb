module BotEngine
  module Actions
    class SetWebhook
      def self.call(token, url)
        Telegram::Bot::Client.run(token) do |bot|
          bot.api.set_webhook(url: url)
        end
      end
    end
  end
end
