module BotEngine
  class Exec
    def self.call(object, args = nil, callback = nil)
      new(object, args, callback).call
    end

    def self.get_me(token)
      Actions::GetMe.call(token)
    end

    def self.get_webhook_info(token)
      Telegram::Bot::Api.new(token).call('getWebhookInfo')
    end

    def self.set_webhook(token, hook)
      url = "#{Rails.application.secrets.api_url}/hooks/telegram/#{hook}"
      Actions::SetWebhook.call(token, url)
    end

    def initialize(object = {}, args = nil, callback = nil)
      @object = object
      @args = args
      @callback = callback
      @current_state = @args[:token]
    end

    def call
      @args[:variables].each { |k, v| @object['text'].gsub!(/{{#{k}}}/, v) } if @object['text']
      case @object['component_type']
      when 'Components::Message'
        BotEngine::Actions::SendMessage.send_message(@args[:token], @args[:channel], @object['text'], @args[:markup])
      when 'Components::Keyboard'
        keyboard = BotEngine::Tg::Objects::Keyboard.construct(@object)
        BotEngine::Actions::SendMessage.send_message(@args[:token], @args[:channel], @object['text'], keyboard)
      end
    end
  end
end
