module BotEngine
  module Tg
    module Objects
      class Keyboard
        def self.construct(object)
          # "{\"1\"=>{\"1\"=>\"Line1, Button1\", \"2\"=>\"Line1, Button2\"}, \"2\"=>\"Line2, Button1"}"
          keyboard = JSON.parse(object['data'].gsub('=>', ':'))
          buttons = keyboard.map do |_n, line|
            if line.is_a?(Hash)
              line.map { |_n, button| construct_button(button) }
            else
              construct_button(line)
            end
          end
          object['data'] = buttons
          construct_keyboard(object)
        end

        private

        def self.construct_button(data)
          Telegram::Bot::Types::KeyboardButton.new(text: data)
        end

        def self.construct_keyboard(object)
          resize_keyboard = object['resize_keyboard'] == 'true'
          one_time_keyboard = object['one_time_keyboard'] == 'true'
          Telegram::Bot::Types::ReplyKeyboardMarkup.new(keyboard: object['data'], resize_keyboard: resize_keyboard, one_time_keyboard: one_time_keyboard)
        end
      end
    end
  end
end
