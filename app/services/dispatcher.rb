class Dispatcher
  def self.update(hook, response, db = 0)
    new(hook, response, db).update
  end

  def self.reset_bot(hook)
    Memcache::Bucket.reset_bot(hook)
  end

  def initialize(hook, update, db = 0)
    @hook = hook
    @update = extract_update(update)
    @redis = Memcache::Bucket.new(@hook, @update)
    @db = db
  end

  def update
    stop = false
    begin
      if @redis.bot_cached?
        args = { channel: @update[:chat][:id],
                 user_id: @update[:from][:id],
                 token: @redis.bot_token,
                 variables: @redis.current_variables }
        component = @redis.next_step
        stop = component['stop'] == 'true'
        BotEngine::Exec.call(component, args)
      end
    rescue => e
      print "\n\n #{pp e} \n\n"
    end until stop
  end

  private

  def extract_update(update)
    update_type = update.attributes.find { |k, v| k != :update_id && v }.first
    update[update_type]
  end
end
