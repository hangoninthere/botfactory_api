module Memcache
  class BotCacher < Engine
    attr_reader :cached

    def initialize(redis, hook)
      @redis = redis
      @hook = hook
      @cached = cached?
    end

    def reload_bot
      load_to_cache
    end

    private

    def cached?
      load_to_cache until @redis.hexists(NAMES::BOT, 'token')
      true
    end

    def load_to_cache
      bot = Bot.find_by(hook: @hook)
      all_screens_elements = bot.screens.map { |screen| [screen.id, screen.screen_elements] }

      # Load Bot data to hash
      load_bot_data(extract_columns(bot))

      # Load all Screens to sorted set
      load_all_screens(bot.screens)

      # Load all screen elements to sorted set
      load_screen_elements(all_screens_elements)

      # Load screen element data to hash
      load_components_data(all_screens_elements)
    end

    def load_bot_data(data)
      load_hash(NAMES::BOT, data)
    end

    def load_all_screens(screens)
      load_arr_to_sorted_set(NAMES::SCREENS,
                             screens.map { |s| [s.priority, "#{NAMES::SCREEN}#{s.id}"] })
    end

    def load_screen_elements(data)
      data.each do |screen|
        load_arr_to_sorted_set("#{NAMES::SCREEN}#{screen[0]}",
                               screen[1].map { |el| [el.priority, "#{NAMES::COMPONENT}#{el.id}"] })
      end
    end

    def load_components_data(data)
      data.each do |screen|
        screen[1].each do |el|
          screen_el_data = extract_columns(el)
          component = el.component
          component_data = extract_columns(component)
          merged_component = component_data.merge(screen_el_data)
          load_hash("#{NAMES::COMPONENT}#{el.id}", merged_component)
        end
      end
    end

    def extract_columns(object)
      data = object.class.column_names.map { |column| [column, object.send(column)] }.to_h
      sanitize(data)
    end

    def sanitize(data)
      data.delete_if { |k, _v| %w[created_at updated_at].include?(k) }
    end
  end
end
