module Memcache
  class Bucket < Engine
    attr_reader :cacher

    def self.reset_bot(hook)
      redis = Redis::Namespace.new(hook, redis: Redis.new)
      keys = redis.keys('*')
      redis.del(keys) until keys.empty?
    end

    def initialize(hook, extracted_update, redis_db = 0)
      redis_connection = Redis.new(db: redis_db)
      @redis = Redis::Namespace.new(hook, redis: redis_connection)
      @hook = hook
      @cacher = BotCacher.new(@redis, @hook)
      @state = UserState.new(@redis, extracted_update)
    end

    def bot_cached?
      @cacher.cached
    end

    def reload_bot
      @cacher.reload_bot
    end

    def next_step
      @state.what_next?
    end

    def bot_token
      @redis.hmget(NAMES::BOT, 'token')[0]
    end

    def current_variables
      @state.variables
    end
  end
end
