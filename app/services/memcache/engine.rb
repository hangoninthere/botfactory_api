module Memcache
  class Engine
    private

      def key_exist?(key_name)
        @redis.exists(key_name)
      end

      ########
      # Hashes
      ########
      def get_hash(field_name)
        @redis.hgetall(field_name)
      end

      def get_hash_value(field_name, key)
        @redis.hmget(field_name, key)[0]
      end

      def load_hash(field_name, hash)
        @redis.multi do
          hash.each do |key, value|
            @redis.hmset(field_name, key, value)
          end
        end
      end

      ######
      # Sets
      ######
      def load_arr_to_set(field_name, arr)
        @redis.multi do
          arr.each do |element|
            @redis.sadd(field_name, element)
          end
        end
      end

      #############
      # Sorted sets
      #############
      def load_arr_to_sorted_set(field_name, arr)
        @redis.multi do
          arr.each do |element|
            @redis.zadd(field_name, element[0], element[1])
          end
        end
      end

      def zcount(field_name)
        @redis.zcount(field_name, '-inf', '+inf')
      end

      def zrangebyscore(field_name, from = '-inf', to = '+inf')
        @redis.zrangebyscore(field_name, from, to)
      end

      #######
      # Lists
      #######
      def load_arr_to_list(field_name, arr)
        @redis.multi do
          arr.each do |element|
            @redis.lpush(field_name, element)
          end
        end
      end
  end
end
