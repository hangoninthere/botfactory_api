module Memcache
  class NAMES
    ALL_NAMES         = "".freeze,
    BOT          = 'Bot'.freeze,
    SCREENS   = 'Screens'.freeze,
    COMPONENT = 'Component:'.freeze,
    STATE     = 'State:'.freeze,
    USER_INPUT        = 'Input:'.freeze,
    SCREEN   = 'Screen:'.freeze
  end
end
