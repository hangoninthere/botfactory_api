module Memcache
  class UserState < Engine
    attr_reader :current_component

    def initialize(redis, extracted_update)
      @redis = redis
      @user = extracted_update[:from][:id]
      @text = extracted_update[:text]
    end

    def variables
      get_hash("#{NAMES::USER_INPUT}#{@user}")
    end

    def what_next?
      @current_state = load_current_state
      @current_component = load_current_component
      # Do some background work if needed
      do_work

      # Get all screen components
      screen_elements = sorted_components(@current_component['screen_id'])

      # Find next component
      next_component(screen_elements)

      @current_component
    end

    private

    def next_component(screen_elements)
      current_index = @current_component['priority'].to_i - 1
      if @current_component['component_type'] == 'Components::GoTo'
        goto
      elsif current_index < screen_elements.length - 1
        define_next_component('current_component' => screen_elements[current_index + 1])
      else
        define_next_component('current_component' => screen_elements[0])
      end
    end

    def goto
      score = @current_component['next_screen']
      next_screen = zrangebyscore(NAMES::SCREENS, "(#{score.to_i - 1}", score)[0]
      next_screen_id = next_screen.split(':')[1]
      next_component = sorted_components(next_screen_id)[0]
      define_next_component('screen_id' => next_screen_id,
                            'current_component' => next_component)
    end

    def define_next_component(data)
      load_hash("#{NAMES::STATE}#{@user}", data)
    end

    def load_current_state
      new_state until key_exist?("#{NAMES::STATE}#{@user}")
      get_hash("#{NAMES::STATE}#{@user}")
    end

    def load_current_component
      new_state if @current_state['current_component'].empty?
      get_hash(@current_state['current_component'])
    end

    def first_screen_id
      get_hash_value(NAMES::BOT, 'first_screen')
    end

    def new_state
      first_component = sorted_components(first_screen_id)[0]
      state = { 'user_id' => @user.to_s,
                'screen_id' => first_screen_id,
                'current_component' => first_component }
      load_hash("#{NAMES::STATE}#{@user}", state)
      load_hash("#{NAMES::USER_INPUT}#{@user}", {})
      reload
    end

    def reload
      @current_state = load_current_state
      @current_component = load_current_component
    end

    def do_work
      case @current_component['component_type']
      when 'Components::UserInput'
        key = @current_component['variable_name']
        val = @text
        load_hash("#{NAMES::USER_INPUT}#{@user}", key => val)
      end
    end

    def sorted_components(screen_id)
      zrangebyscore("#{NAMES::SCREEN}#{screen_id}")
    end
  end
end
