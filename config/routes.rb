Rails.application.routes.draw do
  mount_devise_token_auth_for 'Customer', at: 'auth'

  post '/hooks/telegram/:hook' => 'hooks/telegram#update'

  get '/last_customer' => 'customers#last_customer'

  resources :bots, only: %i[index show create update destroy]
  patch '/bots/:id/update_components' => 'bots#update_components'
  resources :screens, only: %i[create destroy]
  resources :screen_elements, only: %i[create]
end
