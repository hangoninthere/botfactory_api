class CreateBots < ActiveRecord::Migration[5.1]
  def change
    create_table :bots do |t|
      t.string :name, default: ''
      t.string :hook, unique: true
      t.string :token, unique: true
      t.integer :first_screen
      t.integer :customer_id, null: false
      t.timestamps
    end
    add_index :bots, :hook, unique: true
  end
end
