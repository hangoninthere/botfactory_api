class CreateComponentsMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :components_messages do |t|
      t.string :text
      t.string :component_type
      t.boolean :stop, default: false
      t.timestamps
    end
  end
end
