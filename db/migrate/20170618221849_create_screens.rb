class CreateScreens < ActiveRecord::Migration[5.1]
  def change
    create_table :screens do |t|
      t.integer :bot_id, null: false
      t.integer :priority, null: false
      t.string :name, default: 'New screen'
      t.boolean :is_first, default: false
      t.timestamps
    end
  end
end
