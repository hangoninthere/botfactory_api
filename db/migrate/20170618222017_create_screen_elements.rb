class CreateScreenElements < ActiveRecord::Migration[5.1]
  def change
    create_table :screen_elements do |t|
      t.string :component_type
      t.integer :component_id
      t.integer :screen_id
      t.integer :priority
      t.timestamps
    end
  end
end
