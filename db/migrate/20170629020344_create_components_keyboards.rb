class CreateComponentsKeyboards < ActiveRecord::Migration[5.1]
  def change
    create_table :components_keyboards do |t|
      t.string :text
      t.string :component_type
      t.boolean :stop, default: true
      t.jsonb :data, default: {}
      t.boolean :resize_keyboard, default: true
      t.boolean :one_time_keyboard, default: true

      t.timestamps
    end
  end
end
