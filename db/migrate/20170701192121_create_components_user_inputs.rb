class CreateComponentsUserInputs < ActiveRecord::Migration[5.1]
  def change
    create_table :components_user_inputs do |t|
      t.text :variable_name
      t.string :component_type
      t.boolean :stop, default: false

      t.timestamps
    end
  end
end
