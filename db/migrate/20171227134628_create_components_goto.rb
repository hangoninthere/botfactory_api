class CreateComponentsGoto < ActiveRecord::Migration[5.1]
  def change
    create_table :components_go_tos do |t|
      t.integer :next_screen, null: false, default: 1
      t.string :component_type
      t.boolean :stop, default: false

      t.timestamps
    end
  end
end
