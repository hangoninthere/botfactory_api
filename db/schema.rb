# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171227134628) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "bots", force: :cascade do |t|
    t.string "name", default: ""
    t.string "hook"
    t.string "token"
    t.integer "first_screen"
    t.integer "customer_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["hook"], name: "index_bots_on_hook", unique: true
  end

  create_table "components_go_tos", force: :cascade do |t|
    t.integer "next_screen", default: 1, null: false
    t.string "component_type"
    t.boolean "stop", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "components_keyboards", force: :cascade do |t|
    t.string "text"
    t.string "component_type"
    t.boolean "stop", default: true
    t.jsonb "data", default: {}
    t.boolean "resize_keyboard", default: true
    t.boolean "one_time_keyboard", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "components_messages", force: :cascade do |t|
    t.string "text"
    t.string "component_type"
    t.boolean "stop", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "components_user_inputs", force: :cascade do |t|
    t.text "variable_name"
    t.string "component_type"
    t.boolean "stop", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "name"
    t.string "nickname"
    t.string "image"
    t.string "email"
    t.json "tokens"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["confirmation_token"], name: "index_customers_on_confirmation_token", unique: true
    t.index ["email"], name: "index_customers_on_email", unique: true
    t.index ["reset_password_token"], name: "index_customers_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_customers_on_uid_and_provider", unique: true
  end

  create_table "screen_elements", force: :cascade do |t|
    t.string "component_type"
    t.integer "component_id"
    t.integer "screen_id"
    t.integer "priority"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "screens", force: :cascade do |t|
    t.integer "bot_id", null: false
    t.integer "priority", null: false
    t.string "name", default: "New screen"
    t.boolean "is_first", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "telegram_id"
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
