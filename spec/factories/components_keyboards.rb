FactoryGirl.define do
  factory :components_keyboard, class: 'Components::Keyboard' do
    text 'MyString'
    component_type 'MyString'
    stop false
    data 'MyString'
  end
end
