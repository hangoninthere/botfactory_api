FactoryGirl.define do
  factory :components_user_input, class: 'Components::UserInput' do
    variable_name 'MyText'
    component_type 'MyText'
    stop false
  end
end
