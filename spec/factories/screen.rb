FactoryGirl.define do
  factory :screen do
    name 'Some screen'
    is_first false
    bot
  end
end
