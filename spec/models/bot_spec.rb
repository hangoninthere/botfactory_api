require 'rails_helper'

describe Bot, type: :model do
  let (:bot) { create :bot }

  describe 'association' do
    it { should have_many :screens }
    it { should belong_to :customer }
  end

  describe 'first screen' do
    it 'should have screens' do
      bot.screens.count > 0
    end

    it 'should return first screen' do
      expect(bot.first_screen).to eq(1)
    end
  end
end
