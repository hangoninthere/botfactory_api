require 'rails_helper'

describe Components::Message, type: :model do
  let(:bot) { create(:bot) }
  let(:screen) { bot.screens.first }
  let(:component_message) { create :component_message }
  let(:screen_component) { screen.screen_elements.create(component: component_message, priority: 1) }

  it { should have_one :screen_element }
end
