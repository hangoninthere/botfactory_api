require 'rails_helper'

describe Screen, type: :model do
  let(:bot) { create(:bot) }
  let(:screen) { bot.screens.first }
  let(:component_message) { create :component_message }
  let(:screen_component) { screen.screen_elements.create(component: component_message, priority: 1) }

  # subject { screen }

  it { should belong_to(:bot) }
  it { should have_many :screen_elements }
  it { should have_many(:messages).through(:screen_elements) }

  it 'should belong_to bot' do
    expect(screen.bot.id).not_to be_nil
  end

  it 'should be first' do
    expect(screen.is_first).to be_truthy
  end
end
