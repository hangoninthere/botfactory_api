require 'rails_helper'
require 'telegram/bot'

describe Dispatcher, type: :service do
  let!(:bot) { create(:bot) }
  let!(:screen) { bot.screens.first }
  let(:hook) { bot.hook }
  let!(:component_message) { create :component_message, stop: true, text: 'Hello from Telegram!' }
  let!(:screen_component) { screen.screen_elements.create(component: component_message, priority: 1) }
  data = { 'update_id' => 433_261_932, 'message' => { 'message_id' => 666, 'from' => { 'id' => 54_069_100, 'first_name' => 'Nico', 'last_name' => 'Freeman', 'username' => 'Nico_Freeman', 'language_code' => 'en-US' }, 'chat' => { 'id' => 54_069_100, 'first_name' => 'Nico', 'last_name' => 'Freeman', 'username' => 'Nico_Freeman', 'type' => 'private' }, 'date' => 1_498_310_855, 'text' => 'Hello from Telegram!' }, 'hook' => @hook.to_s, 'telegram' => { 'update_id' => 433_261_932, 'message' => { 'message_id' => 666, 'from' => { 'id' => 54_069_100, 'first_name' => 'Nico', 'last_name' => 'Freeman', 'username' => 'Nico_Freeman', 'language_code' => 'en-US' }, 'chat' => { 'id' => 54_069_100, 'first_name' => 'Nico', 'last_name' => 'Freeman', 'username' => 'Nico_Freeman', 'type' => 'private' }, 'date' => 1_498_310_855, 'text' => 'pl' } } }
  update = Telegram::Bot::Types::Update.new(data)
  let(:telegram) { double }
  let(:api) { double }

  let!(:bucket) { Memcache::Bucket.new(hook, 54_069_100, '', 5) }

  before(:each) { bucket.clear_db }
  after(:each) { bucket.clear_db }

  it 'should send message' do
    token = bot.token
    channel = update.message.from.id
    text = update.message.text
    markup = nil

    allow(Telegram::Bot::Client).to receive(:run).with(token).and_yield(telegram)
    allow(telegram).to receive_message_chain(:api, :send_message)
    expect(telegram).to receive_message_chain(:api, :send_message).with(chat_id: channel, text: text, reply_markup: markup)

    Dispatcher.update(bot.hook, update, 5)
  end
end
