require 'rails_helper'

describe Memcache::Bucket, type: :model do
  let!(:bot) { create(:bot) }
  let(:hook) { bot.hook }
  let(:screen) { bot.screens.first }
  let(:component_message) { create :component_message }
  let!(:screen_component) { screen.screen_elements.create(component: component_message, priority: 1) }

  let!(:bucket) { Memcache::Bucket.new(hook, 54_069_100, '', 5) }

  before(:each) { bucket.clear_db }
  after(:each) { bucket.clear_db }

  it 'should exist' do
    expect(bucket).to be
  end

  it 'should create bot if not exist' do
    expect(bucket.bot_cached?).to be_truthy
  end

  it 'should return current user state' do
    bucket.bot_cached?
    user_state = bucket.user_state
    expect(user_state['user_id']).to be
  end

  it 'should return next step' do
    bucket.bot_cached?
    next_component = bucket.next_step
    expect(next_component).to be
  end

  it 'should return bot token' do
    bucket.bot_cached?
    token = bucket.bot_token
    expect(token).to eq('Chy6d4A6wwZxUSB')
  end
end
